Clark County Facilities Survey Tool Manual
================

<style type="text/css">

body{ /* Normal  */
      font-size: 18px;
}

div.notes_table + table tr:nth-child(even) {background: #f2f2f2}

div.notes2_table + table tr:nth-child(even) {background: #f2f2f2}

h6 {
  background-color: WhiteSmoke;
  text-align: center;
  display: inline-block; margin-left: 55px;
  border-left-style: solid;
  border-left-color: gray;
  padding: 10px;
}
#demand-score-formula {
  text-indent: 50px;
}
#supply-score-formula {
  text-indent: 50px;
}
#back-to-top {
  text-align: right;
  padding-right: 25px;
  padding-top: 10px;
}

</style>

## Table of Contents

  - <a href="#about-the-Clark-County-Facilities-Tool">About the Clark
    County Facilities Tool</a>
  - <a href="#metric-Calculations-and-Assumptions">Metric Calculations
    and Assumptions</a>
      - <a href="#demand-score">Demand Score</a>
      - <a href="#supply-score">Supply Score</a>
  - <a href="#instructions">Instructions</a>
  - <a href="#tab-overview">Tab Overview</a>
      - <a href="#criteria-selection">Criteria Selection</a>
      - <a href="#map">Map</a>
      - <a href="#regional-demand-table">Regional Demand Table</a>
      - <a href="#facility-supply-table">Facility Supply Table</a>
      - <a href="#add-new-facilities">Add New Facilities</a>
      - <a href="#notes-and-instructions">Notes and Instructions</a>
  - <a href="#sources-and-data-collection">Sources and Data
    Collection</a>

<div id="about-the-Clark-County-Facilities-Tool">

 

</div>

## About the Clark County Facilities Survey Tool

<div id="about-the-Clark-County-Facilities-Tool-2">

 

</div>

  - ***Overall Goal*** : There are two primary goals of the Clark County
    Facilities Survey Tool. The first is to make it easier to identify
    high demand areas of the county where charter operators should
    target the opening of new schools. The second is to provide charter
    operators with an actionable list of facility options to expedite
    their facilities search along with their creation of new high
    quality schools. This tool achieves these goals by -
      - Highlighting the highest demand regions, based on a few selected
        inputs, within a prioritized list and map
      - Presenting nearby viable facility options within their own
        customizable list and also displayed with a rank within the map
  - ***Background*** : As of the creation of this tool, total student
    enrollment in Clark County exceeds the number of seats available in
    public schools and over 1⁄3 of students are enrolled in schools
    performing below the state average. The persistent overcrowding and
    underperformance of schools across Clark County presents a valuable
    opportunity for charter operators to open high-quality schools where
    it matters most.

<div id="back-to-top">

  <a href="#top">Back to top</a>

</div>

<div id="metric-Calculations-and-Assumptions">

 

</div>

## Metric Calculations and Assumptions

<div id="demand-score">

 

</div>

  - ***Demand Score*** : indicates the need for high-quality school
    seats based on the following three factors–
    
      - *School Quality* :  
        Is based on a one-to-five star rating system which indicates
        school performance relative to Nevada state standards. A
        one-star rating indicates that a school *did not meet* state
        performance standards while a five-star rating indicates that a
        school *exceeded* state performance standards. The School
        Quality factor prioritizes lower-quality schools as an indicator
        of demand for higher quality seats.
    
      - *Utilization* :  
        Is the total regional enrollment divided by the overall regional
        school seat capacity. Demand Regions with Utilization rates that
        exceed 100% are considered over-utilized. The Utilization factor
        prioritizes higher utilization rates, as they indicate regions
        where school seat capacity is insufficient to support the
        current student population.
        
        Capacity for projected future years is based on the 2018-19
        school year, as it is the most recent school capacity study
        available.
        
        There are over 2,000 portable classrooms in clark county. These
        portable classrooms are not considered a long term capacity
        solution and have been excluded from school capacity
        calculations.
    
      - *Surplus Enrollment* :  
        Is the difference between school capacity and enrollment. The
        Surplus Enrollment factor prioritizes Demand Regions with
        greater numbers of Surplus Enrollment as it is an indicator of
        the potential enrollment for any new high-quality seats to be
        created.
        
        Like Utilization, the calculation for Surplus Enrollment also
        excludes school capacity for portable classrooms.

<div id="1">

 

</div>

Enrollment projections, which generate projected Utilization and Surplus
Enrollment, are calculated using a rolling weighted enrollment average
from the most recent five years prior to the selected enrollment year
(or the “Select a Year to Open Your School” field). Enrollment for years
closer to the enrollment year selected are more heavily weighted.

<div id="2">

 

</div>

The Demand Score is the sum of the weighted School Quality, Utilization
and Surplus Enrollment demand factors. The weights of these factors
prioritize School Quality and are as follows -

  - School Quality = 60%
  - Utilization = 20%
  - Surplus Enrollment = 20%

<div id="demand-score-formula">

 

<h5>

Demand Score Formula :

</h5>

</div>

<div id="demand-score-formula-formula">

 

<h6>

Demand Score = (School Quality x 60%) + (Utilization x 20%) + (Surplus
Enrollment x 20%)

</h6>

</div>

<div id="supply-score">

 

</div>

  - ***Supply Score*** : indicates the need for high-quality school
    seats based on the following three factors–
      - Cost: Prioritizes facilities with the lowest cost
      - Size: Prioritizes facilities with the greatest amount of space
      - Distance: Prioritizes facilities closest to the Priority School
        selected

<div id="supply-score-formula">

 

<h5>

Supply Score Formula :

</h5>

</div>

<div id="supply-score-formula-formula">

 

<h6>

Supply Score = (Cost x Cost Weight) + (Size x Size Weight) + (Distance x
Distance Weight)

</h6>

</div>

<div id="back-to-top">

  <a href="#top">Back to top</a>

</div>

<div id="instructions">

 

</div>

## Instructions

<div id="instructions-2">

 

</div>

Use the instructions below to first identify high-demand regions of
Clark County and then review potentially viable facilities based on your
search criteria.

  - *Step-by-Step Navigation*
      - **Open to Main Page**
          - In the Criteria Selection Panel (on the left)–
              - Select your Enrollment Year (*the intended opening
                school year*)
              - Select your School Level (*the intended school level to
                serve– ES, MS, HS, or Secondary*)
      - **Select Regional Demand Table tab**
          - Review the list of regions within the Regional Demand Table
            to identify the desired region of demand. To inform this
            decision, consider the Demand Score factors of *Average
            School Quality, Utilization, and Surplus Enrollment.*
          - Once the desired region of demand is identified, select the
            Demand Region in the Criteria Selection Panel.
      - **Select Map tab**
          - Schools are represented as circles and vary in color based
            on a yellow-to-green color ramp. The darker the color, the
            higher the Demand Score. The Demand Score can be checked by
            clicking on a school’s circle marker.
          - Available school facilities are represented as orange
            circles.
          - Based on a preferred location within the Demand Region and
            the Demand Scores of schools within it, identify a school
            nearest to the preferred location. School-level Demand
            Scores can be checked by clicking on their circle markers.
          - Select the identified school in the Criteria Selection Panel
            under “Priority School.”
      - **Select Facility Supply Table tab**
          - Based on the selections in the Criteria Selection Panel, the
            Facility Supply Score and the Priority School Distance have
            updated.
          - There are three Facility Supply Score factors and adjustable
            weights for Cost, Size, and Distance which allows a
            customized facility search. The sum of these weights should
            represent 100% of the Facility Supply Score no matter which
            factor is prioritized.
              - ***Example \#1*** - To prioritize sites with the lowest
                cost, reduce the Facility Supply Score Weights for Size
                and Distance. If Size and Distance are reduced to
                10%/0.10 each (20%/0.20 total), the Cost weight should
                be increased in proportion to 80%/0.8.
              - ***Example \#2*** - To prioritize sites with the most
                space, reduce the Facility Supply Score Weights for Cost
                and Distance. If Cost and Distance are reduced to
                20%/0.20 each, the Size weight should be increased in
                proportion to 60%/0.6.
              - ***Example \#3*** - To prioritize sites closest to the
                Priority School, reduce the Facility Supply Score
                Weights for Cost and Size. If Cost and Size are reduced
                to 5%/0.05 each, the Cost weight should be increased in
                proportion to 90%/0.9.
          - Once the Facility Supply Score factors are adjusted to their
            desired weights, the Facility Supply Table can be further
            narrowed by using the filters above to enter the minimum
            size, maximum cost and distance from the Priority School.
            Please note that facilities missing records related to the
            Supply Score Factors are deprioritized in their ranking
            within the Facility Supply Table.
          - Review the list of potentially viable facilities within the
            Facility Supply Table as well as the Map to become oriented
            to their locations. The top ten ranked facilities will be
            updated within the map as labeled *orange circles*. For
            quick reference, addresses in the Map can be found by
            clicking their orange circle markers. Please note that
            Facilities rank labels reflect the highest facility Supply
            Score located at one address. Rank labels will skip numbers
            where there are more than one listed space at one address
            (e.g. if a set of address has three listed spaces with the
            highest Supply Score, it will be labeled with a one. The
            priority address that follows will be labeled with a four).
          - Use the listed contact information to reach out to agents
            and learn more about these facilities.
          - To save the Facilities Supply Table based on your search
            criteria, click the “Download Facilities Table as CSV”
            button.
      - **Select Add New Facilities tab**
          - If new viable facilities are surfaced (e.g. temporary
            incubation spaces like churches, non-profit or municipal
            facilities) which users would like to include in the tool
            for future searches, complete all fields in the Add New
            Facilities tab. Be sure that all fields are entered into the
            appropriate syntax and exclude extraneous characters.
          - To find longitude and latitude, copy and paste the full
            address into [Google Maps](https://www.google.com/maps).
            Once the location populates onto the screen, right click the
            red pin and select “What’s Here?” A pop-up will prompt the
            longitude and latitude at the bottom. Copy and paste these
            coordinates into their appropriate fields.
          - Once all fields are complete, click the “Submit” button
            which will add the new facility to the tool. Reload the page
            (or the tool) to incorporate the new facility into a new
            search.

<div id="back-to-top">

  <a href="#top">Back to top</a>

</div>

<div id="tab-overview">

 

</div>

## Tab Overview

<div id="criteria-selection">

 

</div>

  - *Criteria Selection*
      - **Select a Year to Open Your School**
          - Selecting a year to open the school populates enrollment
            numbers into the tool to generate corresponding Utilization
            Rates and Surplus Enrollment numbers, which are factors of
            the overall Demand Score. The Demand Score is the key score
            that helps users identify the most viable regions within
            Clark County to create new high-quality seats.
          - If the inaugural school year is known, select that year as
            the Enrollment Year.
      - **Select a School Level to Serve**
          - Selecting a school level will narrow the school demand
            factors to only those serving the school level selected. For
            example, if the user selects Elementary School– School
            Quality Ratings, Utilization Rates, and Surplus Enrollment
            will not be considered from Middle School or High School
            levels.
          - Select the intended School Level to serve.
      - **Select a Demand Region**
          - Selecting a Demand Region will populate the Map with only
            schools from the region selected.
          - Select the Demand Region after reviewing the Regional Demand
            Table and choosing one of the highest-ranking regions based
            on its Demand Score.
      - **Select a Priority School**
          - Selecting a Priority School will anchor the Distance factor
            of the Facility Supply Score to the school location the user
            wishes to be nearest to.
          - Select the Priority School by reviewing the Map. If it is
            unclear as to what school to select, prioritize the school
            with the highest Demand Score.

<div id="map">

 

</div>

  - *Map* :  
    The Map tab enables users to view public schools and potentially
    viable facilities on a map. Users can zoom to their geographic
    region of interest by selecting different Demand Regions to view
    schools and nearby facilities within them.

<div id="regional-demand-table">

 

</div>

  - *Regional Demand Table* :  
    The Regional Demand Table tab displays a table that ranks Demand
    Region based on their Demand Score. As mentioned in the Metric
    Calculations and Assumptions section, the Demand Score is a factor
    of *Average School Quality, Utilization, and Surplus Enrollment*
    within the Demand Region. The Regional Demand Table allows users to
    view these factors as they prioritize Demand Regions in which to
    focus their facilities search.

<div id="facility-supply-table">

 

</div>

  - *Facility Supply Table* :  
    The Facility Supply Table tab reorders and ranks facilities based on
    the Supply Score factors the user selects. Users can adjust the
    weights of the Supply Score factors (*Cost, Size, and Distance*)
    based on facility needs. Additionally, users can set parameters on
    these factors to further narrow the table’s scope (*minimum size,
    maximum cost and distance*).

<div id="add-new-facilities">

 

</div>

  - *Add New Facilities* :  
    The Add New Facilities tab allows users to add new facilities to
    include in the tool for further consideration and so that future
    users can view as well.

<div id="notes-and-instructions">

 

</div>

  - *Notes and Instructions* : The Notes and Instructions tab provides
    information about the Clark County Facilities Survey Tool, including
    information on the Survey Tool’s Overall Goal, Background,
    Instructions, Tab Overview, Sources, as well as Metric Calculations
    and Assumptions.

<div id="back-to-top">

  <a href="#top">Back to top</a>

</div>

<div id="sources-and-data-collection">

 

</div>

## Sources and Data Collection

<div id="sources-and-data-collection-2">

 

</div>

The data collected from this tool was sourced from–

<div id="0180">

 

</div>

  - O180 Database:
      - *School Characteristic Data - School Names, School Level, School
        Type, and Quality Data*

<div id="clark-county-demographics&quot;">

 

</div>

  - [Clark County Demographics, Zoning and Geographic Information
    Systems](http://dzg.ccsd.net/):
      - *Demand Regions* -2018-19 High School Zones Data
      - *Historical School Capacity Data - 2008-09 through 2018-19
        School Capacity Studies*

<div id="nevada-state-report-card">

 

</div>

  - [Nevada State Report Card](http://nevadareportcard.nv.gov/DI/):
      - *2008-09 through 2017-18 Historical Enrollment*

<div id="42floors">

 

</div>

  - [42Floors](https://42floors.com/us/nv/las-vegas-paradise):
      - *Commercial Real Estate Listings (last pulled 9/17/2019)*

<div id="back-to-top">

  <a href="#top">Back to top</a>

</div>
